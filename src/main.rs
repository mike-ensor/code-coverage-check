fn main() {
    println!("Welcome, this is a default starter message");
}

pub fn area(width: i32, height: i32) -> i32 {
    return width*height;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn main_test() {
        assert_eq!(4, area(2,2));
    }
}