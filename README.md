# Overview

The purpose of this utility is to read code-coverage reports output from various languages and pass or fail an execution based on a policy.

Rules that are embedded inside of POM files (or other source) limit a platform engineer from adjusting or growing policies over time without direct code-level integration. The focus is to allow teams to use externalized policies-as-code.

## Story/Use Cases
1. As a security or compliance admin, I want to define policies and apply them during build-time processes to assist in securing software supply chains.

## Usage

```bash
code-coverage --policy app-rules.yaml --report target/coverage.csv
```

## Policy
This will be a YAML based policy following similar (or the same) entries as "jacoco" rules.

